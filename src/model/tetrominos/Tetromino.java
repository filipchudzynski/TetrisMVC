/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.tetrominos;

import model.Model.cellContent;

/**
 *
 * @author Filip
 * Interface class for Tetrominos
 */
public interface Tetromino {
    /**
     * rotates Tetromino
     */
    void rotate();
    /**
     * makes the Tetromino fall down 
     */
    void fall();
    /**
     * initiate the Tetromino to be used
     * @throws Exception 
     */
    void init() throws Exception;
    /**
     * lets the user move down the Tetromino with keyboard
     */
    void moveDown();
    /**
     * moves the tetromino left
     */
    void moveLeft();
    /**
     * moves the tetromino right
     */
    void moveRight();
    
    /**
    * enum class, which serves as an information about the state of tetromino
    */
    public enum Rotation{
        identity,one,two,three;
    }
    public cellContent[][] getRepresentation();
}
