/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.tetrominos;

import model.Model;

/**
 *
 * @author Filip class implements some usefull methods for tetrominos. It allows
 * to decrease the redundancies in code
 */
abstract class Moveable {

    private Model.cellContent[][] grid;
    private Model model;
    private Point[] pos = new Point[4];
    private Model.cellContent color = Model.cellContent.yellow;

    protected Moveable(Model model) {
        this.model = model;
        this.grid = model.getGrid();
    }

    ;
    /**
     * setPos sets position to given argument
     * @param pos 
     */
    public void setPos(Point[] pos) {
        this.pos = pos;
    }

    /**
     * setPos sets color to given argument
     *
     * @param color
     */

    public void setColor(Model.cellContent color) {
        this.color = color;
    }

    /**
     * method which is supposed to be overriden and implement when tetromino
     * should move
     *
     * @param direction of movement
     * @return always true
     */
    protected boolean canMove(String direction) {
        return true;
    }

    ;
    /**
     * method clears previous position from the game grid
     */
    final void removePreviousPosition() {
        grid[pos[0].y][pos[0].x] = Model.cellContent.empty;
        grid[pos[1].y][pos[1].x] = Model.cellContent.empty;
        grid[pos[2].y][pos[2].x] = Model.cellContent.empty;
        grid[pos[3].y][pos[3].x] = Model.cellContent.empty;
    }

    /**
     * method updates new position to the game grid
     */
    final void updatePosition() {
        grid[pos[0].y][pos[0].x] = color;
        grid[pos[1].y][pos[1].x] = color;
        grid[pos[2].y][pos[2].x] = color;
        grid[pos[3].y][pos[3].x] = color;
    }

    /**
     * method moves pt.x in x axis a x
     */
    final void moveX(int x) {
        for (Point pt : pos) {
            pt.x += x;
        }
    }

    /**
     * method moves pt.y in y axis a y
     */
    final void moveY(int y) {
        for (Point pt : pos) {
            pt.y += y;
        }
    }
}
