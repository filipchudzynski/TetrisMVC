/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.tetrominos;

/**
 * helper class, which represents point in the game grid
 * @author Filip
 */
public class Point {

    int x;
    int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    void setPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
