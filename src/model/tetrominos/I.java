/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.tetrominos;

import controller.Controller;
import model.Model;
import model.Model.cellContent;

/**
 * class which represents I Tetromino
 *
 * @author Filip
 */
public class I implements Tetromino {

    private cellContent[][] grid;
    private static cellContent color = cellContent.yellow, empty = cellContent.empty;
    private static cellContent[][] tetrominoRepresentation = {{empty, color, empty, empty}, {empty, color, empty, empty},
    {empty, color, empty, empty}, {empty, color, empty, empty}};
    private Rotation rotation = Rotation.identity;
    private Model model;
    private Point[] pos = new Point[4];

    public I(Model model) throws Exception {
        this.model = model;
        this.grid = model.getGrid();
    }

    @Override
    public void init() throws Exception {
        if (grid[0][4] == cellContent.empty && grid[1][4] == cellContent.empty) {
            grid[0][4] = color;
            grid[1][4] = color;
            grid[2][4] = color;
            grid[3][4] = color;
            pos[0] = new Point(4, 0);                   //[0] when vertical is on the top [3] on the bottom
            pos[1] = new Point(4, 1);
            pos[2] = new Point(4, 2);
            pos[3] = new Point(4, 3);                   //[0] when horizontal is on the left [3] is on the right
            model.updateModel(grid);
        } else {
            model.gridFilled();
            throw new Exception(" Grid is filled ");
        }
    }

    @Override
    public void rotate() {
        switch (rotation) {
            case identity:
                if (pos[2].x > 1
                        && grid[pos[2].y][pos[2].x - 2] == cellContent.empty && grid[pos[2].y][pos[2].x - 1] == cellContent.empty
                        && pos[2].x != model.columns - 1 && grid[pos[2].y][pos[2].x + 1] == cellContent.empty) {
                    rotation = Rotation.one;
                    removePreviousPosition();
//                if (pos[2].x < 2 || grid[pos[2].y][pos[2].x-3] != cellContent.empty) {
//                   // moveX(2 - pos[0].x);
//                } else if (pos[0].x > model.columns - 2|| grid[pos[2].y][pos[2].x+2] != cellContent.empty) {
//                   // moveX(-1);
//                }
                    pos[0].setPoint(pos[0].x - 2, pos[0].y + 2);    //|
                    pos[1].setPoint(pos[1].x - 1, pos[1].y + 1);
                    pos[2].setPoint(pos[2].x, pos[2].y);
                    pos[3].setPoint(pos[3].x + 1, pos[3].y - 1);
                }
                break;
            case one:
                if (pos[1].y < model.rows - 1 && grid[pos[1].y + 1][pos[1].x] == cellContent.empty
                        && grid[pos[1].y - 1][pos[1].x] == cellContent.empty && grid[pos[1].y - 2][pos[1].x] == cellContent.empty) {
                    rotation = Rotation.two;
                    removePreviousPosition();

                    pos[0].setPoint(pos[0].x + 1, pos[0].y - 2);    //_
                    pos[1].setPoint(pos[1].x, pos[1].y - 1);
                    pos[2].setPoint(pos[2].x - 1, pos[2].y);
                    pos[3].setPoint(pos[3].x - 2, pos[3].y + 1);
                }
                break;
            case two:
                if (pos[1].x != 0
                        && grid[pos[2].y][pos[2].x - 1] == cellContent.empty && pos[1].x < model.columns - 2
                        && grid[pos[2].y][pos[2].x + 2] == cellContent.empty && grid[pos[2].y][pos[2].x + 1] == cellContent.empty) {
                    rotation = Rotation.three;
                    removePreviousPosition();
//                if (pos[0].x < 1) {
//                    moveX(1);
//                } else if (pos[0].x > model.columns - 3) {
//                    moveX((model.columns - 1) - (pos[0].x + 2));
//                }
                    pos[0].setPoint(pos[0].x - 1, pos[0].y + 1);    // |
                    pos[1].setPoint(pos[1].x, pos[1].y);
                    pos[2].setPoint(pos[2].x + 1, pos[2].y - 1);
                    pos[3].setPoint(pos[3].x + 2, pos[3].y - 2);
                }
                break;
            case three:
                if (pos[2].y < model.rows - 2
                        && grid[pos[2].y + 1][pos[2].x] == cellContent.empty && grid[pos[2].y + 2][pos[2].x] == cellContent.empty
                        && grid[pos[2].y - 1][pos[2].x] == cellContent.empty) {
                    rotation = Rotation.identity;
                    removePreviousPosition();

                    pos[0].setPoint(pos[0].x + 2, pos[0].y - 1);    //-
                    pos[1].setPoint(pos[1].x + 1, pos[1].y);
                    pos[2].setPoint(pos[2].x, pos[2].y + 1);
                    pos[3].setPoint(pos[3].x - 1, pos[3].y + 2);
                }
                break;
        }
        updatePosition();
        model.updateModel(grid);

    }

    @Override
    public void fall() {
        if (canMove("down")) {
            removePreviousPosition();
            moveY(1);
            updatePosition();
            model.updateModel(grid);
        } else {
            model.tetrominoReachedBottom();//TODO Emit signal/generate event tetromino reached bottom
        }

    }

    @Override
    public void moveDown() {

        if (canMove("down")) {
            removePreviousPosition();
            moveY(1);
            updatePosition();
            model.updateModel(grid);
        } else {
            model.tetrominoReachedBottom();//TODO Emit signal/generate event tetromino reached bottom
        }

    }

    @Override
    public void moveLeft() {
        if (canMove("left")) {
            removePreviousPosition();
            moveX(-1);
            updatePosition();
            model.updateModel(grid);
        }
    }

    @Override
    public void moveRight() {
        if (canMove("right")) {
            removePreviousPosition();
            moveX(1);
            updatePosition();
            model.updateModel(grid);
        }
    }

    private void removePreviousPosition() {
        grid[pos[0].y][pos[0].x] = cellContent.empty;
        grid[pos[1].y][pos[1].x] = cellContent.empty;
        grid[pos[2].y][pos[2].x] = cellContent.empty;
        grid[pos[3].y][pos[3].x] = cellContent.empty;
    }

    private void updatePosition() {
        grid[pos[0].y][pos[0].x] = color;
        grid[pos[1].y][pos[1].x] = color;
        grid[pos[2].y][pos[2].x] = color;
        grid[pos[3].y][pos[3].x] = color;
    }

    private void moveX(int x) {
        for (Point pt : pos) {
            pt.x += x;
        }
    }

    private void moveY(int y) {
        for (Point pt : pos) {
            pt.y += y;
        }
    }

    private boolean canMove(String direction) {
        switch (direction) {
            case "left":
                if (pos[0].x > 0) {
                    if (rotation == Rotation.identity || rotation == Rotation.two) {
                        for (int i = 0; i < 4; ++i) {
                            if (grid[pos[i].y][pos[i].x - 1] != cellContent.empty) {
                                return false;
                            }
                        }
                    }
                    if (rotation == Rotation.one || rotation == Rotation.three) {
                        if (grid[pos[0].y][pos[0].x - 1] != cellContent.empty) {
                            return false;
                        }
                    }
                } else {
                    return false;
                }
                break;
            case "right":
                if (pos[3].x < model.columns - 1) {
                    if (rotation == Rotation.identity || rotation == Rotation.two) {
                        for (int i = 0; i < 4; ++i) {
                            if (grid[pos[i].y][pos[i].x + 1] != cellContent.empty) {
                                return false;
                            }
                        }
                    }
                    if (rotation == Rotation.one || rotation == Rotation.three) {
                        if (grid[pos[3].y][pos[3].x + 1] != cellContent.empty) {
                            return false;
                        }
                    }
                } else {
                    return false;
                }
                break;
            case "down":
                if (pos[3].y < model.rows - 1) {
                    if (rotation == Rotation.identity || rotation == Rotation.two) {
                        if (grid[pos[3].y + 1][pos[3].x] != cellContent.empty) {
                            return false;
                        }
                    }
                    if (rotation == Rotation.one || rotation == Rotation.three) {
                        for (int i = 0; i < 4; ++i) {
                            if (grid[pos[i].y + 1][pos[i].x] != cellContent.empty) {
                                return false;
                            }
                        }
                    }
                } else {
                    return false;
                }
                break;

        }

        return true;
    }

    @Override
    public cellContent[][] getRepresentation() {
        return tetrominoRepresentation;
    }

}
