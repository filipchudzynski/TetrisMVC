/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.tetrominos;

import java.util.Random;
import model.Model;

/**
 *
 * @author Filip Class which allows to draw tetrominos
 *
 */
public class TetrominoFactory {

    private Model model;

    public TetrominoFactory(Model model) {
        this.model = model;
    }

    /**
     * Method to get random tetromino
     *
     * @return Tetromino
     * @throws Exception when no more tetromino can be fitted on the game board
     */
    public Tetromino produceRandomTetromino() throws Exception {
        Random rand = new Random();
        Tetromino tetromino = null;
        int tetrominoIndex = rand.nextInt(7);

        switch (tetrominoIndex) {
            case 0:
                tetromino = new I(model);
                break;
            case 6:
                tetromino = new J(model);
                break;
            case 5:
                tetromino = new L(model);
                break;
            case 4:
                tetromino = new O(model);
                break;
            case 1:
                tetromino = new S(model);
                break;
            case 2:
                tetromino = new T(model);
                break;
            case 3:
                tetromino = new Z(model);
                break;
        }
        return tetromino;
    }
}
