/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.tetrominos;

import model.Model;
import model.Model.cellContent;
import model.tetrominos.Tetromino.Rotation;

/**
 * class which represents T Tetromino
 *
 * @author Filip
 */
public class T extends Moveable implements Tetromino {

    private Model.cellContent[][] grid;
    private static cellContent color = cellContent.brightBlue, empty = cellContent.empty;
    private static cellContent[][] tetrominoRepresentation = {{empty, empty, empty, empty}, {empty, empty, color, empty},
    {empty, color, color, color}, {empty, empty, empty, empty}};
    private Tetromino.Rotation rotation = Tetromino.Rotation.identity;
    private Model model;
    private Point[] pos = new Point[4];

    public T(Model model) throws Exception {
        super(model);
        this.model = model;
        this.grid = model.getGrid();
    }

    @Override
    public void init() throws Exception {
        if (grid[0][4] == Model.cellContent.empty && grid[1][4] == Model.cellContent.empty
                && grid[1][3] == Model.cellContent.empty && grid[1][5] == Model.cellContent.empty) {
            grid[1][3] = color;
            grid[0][4] = color;
            grid[1][4] = color;
            grid[1][5] = color;
            pos[0] = new Point(4, 0);                   //[0] when vertical is on the top [3] on the bottom
            pos[1] = new Point(3, 1);
            pos[2] = new Point(4, 1);
            pos[3] = new Point(5, 1);                   //[0] when horizontal is on the left [3] is on the right
            model.updateModel(grid);
        } else {
            model.gridFilled();
            throw new Exception(" Grid is filled ");
        }
        super.setPos(pos);
        super.setColor(color);
    }

    public void rotate() {
        switch (rotation) {
            case identity:
                if (pos[2].y < model.rows - 1 && grid[pos[2].y + 1][pos[2].x] == cellContent.empty) {
                    rotation = Rotation.one;
                    removePreviousPosition();
                    pos[0].setPoint(pos[0].x, pos[0].y);    //|
                    pos[1].setPoint(pos[1].x + 1, pos[1].y);
                    pos[2].setPoint(pos[2].x + 1, pos[2].y);
                    pos[3].setPoint(pos[3].x - 1, pos[3].y + 1);
                }
                break;
            case one:
                if (pos[1].x > 0 && grid[pos[1].y][pos[1].x - 1] == cellContent.empty) {
                    rotation = Rotation.two;
                    removePreviousPosition();
                    pos[0].setPoint(pos[0].x - 1, pos[0].y + 1);    //|
                    pos[1].setPoint(pos[1].x, pos[1].y);
                    pos[2].setPoint(pos[2].x, pos[2].y);
                    pos[3].setPoint(pos[3].x, pos[3].y);
                }
                break;
            case two:
                if (grid[pos[1].y - 1][pos[1].x] == cellContent.empty) {
                    rotation = Rotation.three;
                    removePreviousPosition();
                    pos[0].setPoint(pos[0].x + 1, pos[0].y - 1);    //|
                    pos[1].setPoint(pos[1].x - 1, pos[1].y);
                    pos[2].setPoint(pos[2].x - 1, pos[2].y);
                    pos[3].setPoint(pos[3].x, pos[3].y);
                }
                break;
            case three:
                if (pos[2].x < model.columns - 1 && grid[pos[2].y][pos[2].x + 1] == cellContent.empty) {
                    rotation = Rotation.identity;
                    removePreviousPosition();
                    pos[0].setPoint(pos[0].x, pos[0].y);    //|
                    pos[1].setPoint(pos[1].x, pos[1].y);
                    pos[2].setPoint(pos[2].x, pos[2].y);
                    pos[3].setPoint(pos[3].x + 1, pos[3].y - 1);
                }
                break;

        }
        updatePosition();
        model.updateModel(grid);
    }

    @Override
    public void fall() {
        if (canMove("down")) {
            removePreviousPosition();
            moveY(1);
            updatePosition();
            model.updateModel(grid);
        } else {
            model.tetrominoReachedBottom();//TODO Emit signal/generate event tetromino reached bottom
        }
    }

    @Override
    public void moveDown() {
        if (canMove("down")) {
            removePreviousPosition();
            moveY(1);
            updatePosition();
            model.updateModel(grid);
        } else {
            model.tetrominoReachedBottom();//TODO Emit signal/generate event tetromino reached bottom
        }
    }

    @Override
    public void moveLeft() {
        if (canMove("left")) {
            removePreviousPosition();
            moveX(-1);
            updatePosition();
            model.updateModel(grid);
        }
    }

    @Override
    public void moveRight() {
        if (canMove("right")) {
            removePreviousPosition();
            moveX(1);
            updatePosition();
            model.updateModel(grid);
        }
    }

    @Override
    protected boolean canMove(String direction) {
        switch (direction) {
            case "down":
                if (pos[3].y == model.rows - 1) {
                    return false;
                }
                if (grid[pos[3].y + 1][pos[3].x] != cellContent.empty) {
                    return false;
                }
                switch (rotation) {
                    case identity:
                        if (grid[pos[1].y + 1][pos[1].x] != cellContent.empty
                                || grid[pos[2].y + 1][pos[2].x] != cellContent.empty) {
                            return false;
                        }
                        break;
                    case one:
                        if (grid[pos[2].y + 1][pos[2].x] != cellContent.empty) {
                            return false;
                        }
                        break;
                    case two:
                        if (grid[pos[0].y + 1][pos[0].x] != cellContent.empty
                                || grid[pos[2].y + 1][pos[2].x] != cellContent.empty) {
                            return false;
                        }
                        break;
                    case three:
                        if (grid[pos[1].y + 1][pos[1].x] != cellContent.empty) {
                            return false;
                        }
                        break;
                }
                break;
            case "left":
                switch (rotation) {
                    case identity:
                        if (pos[1].x - 1 < 0 || grid[pos[0].y][pos[0].x - 1] != cellContent.empty
                                || grid[pos[1].y][pos[1].x - 1] != cellContent.empty) {
                            return false;
                        }
                        break;
                    case one:
                        if (pos[1].x - 1 < 0 || grid[pos[0].y][pos[0].x - 1] != cellContent.empty
                                || grid[pos[1].y][pos[1].x - 1] != cellContent.empty
                                || grid[pos[3].y][pos[3].x - 1] != cellContent.empty) {
                            return false;
                        }
                        break;
                    case two:
                        if (pos[0].x - 1 < 0 || grid[pos[0].y][pos[0].x - 1] != cellContent.empty
                                || grid[pos[3].y][pos[3].x - 1] != cellContent.empty) {
                            return false;
                        }
                        break;
                    case three:
                        if (pos[1].x - 1 < 0 || grid[pos[0].y][pos[0].x - 1] != cellContent.empty
                                || grid[pos[1].y][pos[1].x - 1] != cellContent.empty
                                || grid[pos[3].y][pos[3].x - 1] != cellContent.empty) {
                            return false;
                        }
                        break;
                }
                break;
            case "right":
                switch (rotation) {
                    case identity:
                        if (pos[3].x + 1 > model.columns - 1 || grid[pos[0].y][pos[0].x + 1] != cellContent.empty
                                || grid[pos[3].y][pos[3].x + 1] != cellContent.empty) {
                            return false;
                        }
                        break;
                    case one:
                        if (pos[2].x + 1 > model.columns - 1 || grid[pos[0].y][pos[0].x + 1] != cellContent.empty
                                || grid[pos[2].y][pos[2].x + 1] != cellContent.empty
                                || grid[pos[3].y][pos[3].x + 1] != cellContent.empty) {
                            return false;
                        }

                        break;
                    case two:
                        if (pos[2].x + 1 > model.columns - 1 || grid[pos[2].y][pos[2].x + 1] != cellContent.empty
                                || grid[pos[3].y][pos[3].x + 1] != cellContent.empty) {
                            return false;
                        }
                        break;
                    case three:
                        if (pos[2].x + 1 > model.columns - 1 || grid[pos[0].y][pos[0].x + 1] != cellContent.empty
                                || grid[pos[2].y][pos[2].x + 1] != cellContent.empty
                                || grid[pos[3].y][pos[3].x + 1] != cellContent.empty) {
                            return false;
                        }
                        break;
                }
                break;
        }
        return true;
    }

    @Override
    public cellContent[][] getRepresentation() {
        return tetrominoRepresentation;
    }
}
