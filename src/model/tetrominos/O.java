/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.tetrominos;

import model.Model;
import model.Model.cellContent;

/**
 * class which represents O Tetromino
 *
 * @author Filip
 */
public class O implements Tetromino {

    private Model.cellContent[][] grid;
    private static cellContent color = cellContent.red, empty = cellContent.empty;
    private static cellContent[][] tetrominoRepresentation = {{empty, empty, empty, empty}, {empty, color, color, empty},
    {empty, color, color, empty}, {empty, empty, empty, empty}};
    private Model model;
    private Point[] pos = new Point[4];

    public O(Model model) throws Exception {
        this.model = model;
        this.grid = model.getGrid();
    }

    @Override
    public void init() throws Exception {
        if (grid[0][4] == Model.cellContent.empty && grid[0][5] == Model.cellContent.empty) {
            grid[0][4] = color;
            grid[1][4] = color;
            grid[0][5] = color;
            grid[1][5] = color;
            pos[0] = new Point(4, 0);                   //[0] when vertical is on the top [3] on the bottom
            pos[1] = new Point(4, 1);
            pos[2] = new Point(5, 0);
            pos[3] = new Point(5, 1);                   //[0] when horizontal is on the left [3] is on the right
            model.updateModel(grid);
        } else {
            model.gridFilled();
            throw new Exception(" Grid is filled ");
        }
    }

    @Override
    public void rotate() {

    }

    @Override
    public void fall() {
        if (canMove("down")) {
            removePreviousPosition();
            moveY(1);
            updatePosition();
            model.updateModel(grid);
        } else {
            model.tetrominoReachedBottom();//TODO Emit signal/generate event tetromino reached bottom
        }

    }

    @Override
    public void moveDown() {

        if (canMove("down")) {
            removePreviousPosition();
            moveY(1);
            updatePosition();
            model.updateModel(grid);
        } else {
            model.tetrominoReachedBottom();//TODO Emit signal/generate event tetromino reached bottom
        }

    }

    @Override
    public void moveLeft() {

        if (canMove("left")) {
            removePreviousPosition();
            moveX(-1);
            updatePosition();
            model.updateModel(grid);
        }
        // }
    }

    @Override
    public void moveRight() {
        if (canMove("right")) {
            removePreviousPosition();
            moveX(1);
            updatePosition();
            model.updateModel(grid);
        }
    }

    private void removePreviousPosition() {
        grid[pos[0].y][pos[0].x] = Model.cellContent.empty;
        grid[pos[1].y][pos[1].x] = Model.cellContent.empty;
        grid[pos[2].y][pos[2].x] = Model.cellContent.empty;
        grid[pos[3].y][pos[3].x] = Model.cellContent.empty;
    }

    private void updatePosition() {
        grid[pos[0].y][pos[0].x] = color;
        grid[pos[1].y][pos[1].x] = color;
        grid[pos[2].y][pos[2].x] = color;
        grid[pos[3].y][pos[3].x] = color;
    }

    private void moveX(int x) {
        for (Point pt : pos) {
            pt.x += x;
        }
    }

    private void moveY(int y) {
        for (Point pt : pos) {
            pt.y += y;
        }
    }

    private boolean canMove(String direction) {
        switch (direction) {
            case "left":
                if (pos[0].x > 0) {
                    for (int i = 0; i < 2; ++i) {
                        if (grid[pos[i].y][pos[i].x - 1] != Model.cellContent.empty) {
                            return false;
                        }
                    }
                } else {
                    return false;
                }
                break;
            case "right":
                if (pos[3].x < model.columns - 1) {

                    for (int i = 2; i < 4; ++i) {
                        if (grid[pos[i].y][pos[i].x + 1] != Model.cellContent.empty) {
                            return false;
                        }
                    }
                } else {
                    return false;
                }
                break;
            case "down":
                if (pos[1].y < model.rows - 1) {
                    for (int i = 1; i < 4; i = i + 2) {
                        if (grid[pos[i].y + 1][pos[i].x] != Model.cellContent.empty) {
                            return false;
                        }
                    }
                } else {
                    return false;
                }
                break;

        }

        return true;
    }

    @Override
    public cellContent[][] getRepresentation() {
        return tetrominoRepresentation;
    }

}
