

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * class serves as a demo of the game
 * all parts of MVC architecture are constructed
 * @author Filip
 */
import controller.Controller;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import model.Model;
import view.View;

public class Main {

    public static void main(String args[]) {
        Model model = new Model();
        Controller controller = new Controller(model);
        View view = new View(model,controller);
        model.init(view, controller);
        
    }

}
