/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Controller;
import java.awt.Dimension;
import java.awt.Canvas;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;

/**
 * Class which initiates graphics objects such as frame, canvas, buttons etc
 * Serves also as action listener for the button, then sends the signal to 
 * the controller
 * @author Filip
 */
public class Display implements ActionListener {
    Controller controller;
    
    private JFrame frame;
    private Canvas canvas;
    private Canvas displayNextTetromino;
    private JPanel panel;
    private JLabel numberOfLinesLabel;
    private JButton newGameButton,pauseButton;
    
    private String title;
    private int width, height, cellSize, numberOfCells;

    public Display(String title, int width, int height, int cellSize, int numberOfCells, Controller controller) {
        this.title = title;
        this.height = height;
        this.width = width;
        this.cellSize = cellSize;
        this.numberOfCells = numberOfCells;
        this.controller = controller;
        
        createDisplay();
    }
    /**
     * prepares all graphical elements
     * sets the size of the frame to width and height, doesn't allow to resize
     * uses grid layout for the canvas(with the game) and JPanel.
     * Inside the JPanel uses the group layout, which stores the buttons, score
     * and canvas to display next tetromino
     */
    private void createDisplay() {
        frame = new JFrame(title);
        frame.setSize(width, height);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setLayout(new GridLayout());

        canvas = new java.awt.Canvas();
        canvas.setPreferredSize(new Dimension(cellSize * numberOfCells, height));
        canvas.setMaximumSize(new Dimension(cellSize * numberOfCells, height));
        canvas.setMinimumSize(new Dimension(cellSize * numberOfCells, height));
        canvas.setFocusable(false);

        frame.add(canvas);

        panel = new JPanel();
        GroupLayout layout = new GroupLayout(panel);
        panel.setLayout(layout);
        //panel.setLayout(new FlowLayout());
        panel.setFocusable(false);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        frame.add(panel);

        getFrame().pack();

//        Insets insets = frame.insets();
        JLabel linesLabel = new JLabel("Score:");
        numberOfLinesLabel = new JLabel("0");
        newGameButton = new JButton("New Game");
        newGameButton.addActionListener(this);
        controller.setButton(newGameButton);
        newGameButton.setActionCommand("New Game");
        newGameButton.setEnabled(false);
        pauseButton = new JButton("Pause");
        pauseButton.addActionListener(this);
        pauseButton.setActionCommand("Pause");        
        
        displayNextTetromino = new java.awt.Canvas();
        displayNextTetromino.setPreferredSize(new Dimension(cellSize*4, cellSize*4));
        displayNextTetromino.setMaximumSize(new Dimension(cellSize*4, cellSize*4));
        displayNextTetromino.setMinimumSize(new Dimension(cellSize*4, cellSize*4));
        displayNextTetromino.setFocusable(false);
        newGameButton.setFont(new Font("Serif", Font.PLAIN, 20));
        pauseButton.setFont(new Font("Serif", Font.PLAIN, 20));
        linesLabel.setFont(new Font("Serif", Font.PLAIN, 24));
        numberOfLinesLabel.setFont(new Font("Serif", Font.PLAIN, 24));
        layout.setHorizontalGroup(
                layout.createParallelGroup()
                                .addComponent(newGameButton,0,GroupLayout.DEFAULT_SIZE,width)
                                .addComponent(pauseButton,0,GroupLayout.DEFAULT_SIZE,width)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(linesLabel)
                                .addComponent(numberOfLinesLabel))
                        .addGroup(layout.createSequentialGroup()    
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,
                     width/6, width/6)
                                .addComponent(displayNextTetromino))//)
        );
        layout.setVerticalGroup(
                layout.createSequentialGroup()
                                .addComponent(newGameButton)
                                .addComponent(pauseButton)  
                        .addGroup(layout.createParallelGroup()
                        .addComponent(linesLabel)
                        .addComponent(numberOfLinesLabel)
                        )
                        .addComponent(displayNextTetromino)
                
                        
        );
        newGameButton.setFocusable(false);
        pauseButton.setFocusable(false);

        frame.setVisible(true);
    }

    /**
     * @return the frame
     */
    public JFrame getFrame() {
        return frame;
    }
    
    public Canvas getDisplayNextTetromino(){
        return displayNextTetromino;
    }
    /**
     * notify the label to change the score
     * @param numberOfLines 
     */
    public void setNumberOfLines(int numberOfLines){
        numberOfLinesLabel.setText(Integer.toString( numberOfLines ));
    }

    /**
     * @return the canvas
     */
    public Canvas getCanvas() {
        return canvas;
    }
    /**
     * handles the actions performed on the buttons
     * @param ae 
     */
    @Override
    public void actionPerformed(ActionEvent ae) {
        if("New Game".equals(ae.getActionCommand())){
            controller.startNewGame();
            newGameButton.setEnabled(false);
        }
        else if("Pause".equals(ae.getActionCommand()))
            controller.setPause();
    }
}
