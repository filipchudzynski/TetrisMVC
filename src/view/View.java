/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Controller;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import javax.swing.JOptionPane;
import model.Model;
import model.Model.cellContent;

/**
 *
 * @author Filip Class which is responsible for rendering the graphics of the
 * game. Part of MVC architecture.
 */
public class View {

    private Model model;
    private Controller controller;
    private int width = 640, height = 800, cellSize = 40, numberOfCells = 10;

    private Display display;
    private Graphics g;
    private BufferStrategy bs;

    private cellContent[][] grid;

    /**
     * Constructor takes objecct of Model and Controler as a paramateres. They
     * serve as a handlers to send data between MVC. Also here object of Display
     * is created. Frame(constructed in display) serves as a listner for
     * keyboard. Finally, assets are created preparing spritesheets to be
     * displayed.
     *
     * @param model object of Model(MVC)
     * @param controller object of Controller(MVC)
     */
    public View(Model model, Controller controller) {
        this.model = model;
        this.controller = controller;
        grid = model.getGrid();
        this.controller = controller;
        this.display = new Display("Tetris", width, height, cellSize, numberOfCells, controller);
        display.getFrame().addKeyListener(Controller.getKeyController());
        Assets.init();

    }

    /**
     * render() is method responsible for drawing the content on the game board
     * periodically.
     * It uses buffer to display graphics with no drops in framerate.
     * Using grid from model, render converts abstract data and send them to
     * Graphics object to render them in proper way.
     */
    public void render() {
        bs = display.getCanvas().getBufferStrategy();
        if (bs == null) {
            display.getCanvas().createBufferStrategy(3);
            return;
        }
        g = bs.getDrawGraphics();
        //Clean
        g.clearRect(0, 0, width, height);
        for (int i = 0; i < model.rows; ++i) {
            for (int j = 0; j < model.columns; ++j) {
                g.drawImage(Assets.getCellImage(grid[i][j]), cellSize * j, cellSize * i, cellSize, cellSize, null);
            }
        }

        bs.show();
        g.dispose();
    }

    public View getView() {
        return this;
    }

    public void setModel(Model model) {
        this.model = model;
    }
    /**
     * Method which allows to render preview of the next Tetromino on the
     * right side of the screen.
     * @param previewGrid 9x9 matrix which represents next Tetromino 
     */

    public void renderNextTetromino(cellContent[][] previewGrid) {

        Graphics g2 = display.getDisplayNextTetromino().getGraphics();

        g2.clearRect(0, 0, 75, height / 2);
        for (int i = 0; i < 4; ++i) {
            for (int j = 0; j < 4; ++j) {
                g2.drawImage(Assets.getCellImage(previewGrid[i][j]), cellSize * j, cellSize * i, cellSize, cellSize, null);
            }
        }
        g2.dispose();
    }

    /**
     * Update contents in the grid
     */
    public void updateGrid() {
        this.grid = model.getGrid();
    }
    /**
     * sends information to display to change the score
     * @param numberOfLines  
     */
    public void updateNumberOfLines(int numberOfLines) {
        display.setNumberOfLines(numberOfLines);
    }

    /**
     * Launch the dialog to notify that game is over.
     */
    public void notifyGameOver() {
        JOptionPane.showMessageDialog(display.getFrame(), "Game Over");
    }

}
