/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.image.BufferedImage;

/**
 * 
 * @author Filip
 */
public class SpriteSheet {
    private BufferedImage sheet;
    public SpriteSheet(BufferedImage sheet){
        this.sheet = sheet;
    }
    /**
     * crops the image
     * @param x begginnig of the crop in x axis
     * @param y beggining of the crop in y axis
     * @param width
     * @param height
     * @return croped image
     */
    public BufferedImage crop(int x, int y, int width, int height){
        return sheet.getSubimage(x, y, width, height);  
    }
}
