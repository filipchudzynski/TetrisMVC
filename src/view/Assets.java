/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.image.BufferedImage;
import model.Model.cellContent;

/**
 * Assets are used to store the images, which are being displayed in the grid
 * @author Filip
 */
public class Assets {

    private static final int height = 10, width = 10;
    public static BufferedImage red, green, brightBlue, violet, yellow, darkBlue, brown, empty;
    /**
     * Loads images from the png file to variables using SpriteSheet class
     */
    public static void init() {
        SpriteSheet sheet = new SpriteSheet(ImageLoader.loadImage("/textures/klocki.png"));
        brightBlue = sheet.crop(0, 0, width, height);
        red = sheet.crop(width, 0, width, height);
        green = sheet.crop(2 * width, 0, width, height);
        violet = sheet.crop(3 * width, 0, width, height);
        yellow = sheet.crop(4 * width, 0, width, height);
        darkBlue = sheet.crop(5 * width, 0, width, height);
        brown = sheet.crop(6 * width, 0, width, height);
        empty = sheet.crop(7 * width, 0, width, height);
    }
    /**
     * method to obtain graphical representation of the abstract cell contents
     * @param cell enum which stores the abstract content of the cells in grid
     * @return BufferedImage of the cell based on enum cellContent 
     */
    public static BufferedImage getCellImage(cellContent cell) {
        BufferedImage temp;
        switch (cell) {
            case red:
                temp = red;
                break;
            case green:
                temp = green;
                break;
            case brightBlue:
                temp = brightBlue;
                break;
            case violet:
                temp = violet;
                break;
            case yellow:
                temp = yellow;
                break;
            case darkBlue:
                temp = darkBlue;
                break;
            case brown:
                temp = brown;
                break;
            default:
                temp = empty;
                break;
        }
        return temp;

    }

}
