/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import javax.swing.JButton;
import model.Model;
import model.tetrominos.Tetromino;
import view.View;

/**
 *
 * @author Filip
 */
public class Controller {

    private Model model;
    private View view;
    private static KeyController keyController;

    private JButton newGameButton;
    static boolean restart = false;

    /**
     * constructs the keyController, which takes care of managing keyboard input
     *
     * @param model handler to Model object(MVC)
     */
    public Controller(Model model) {
        this.model = model;
        keyController = Controller.getKeyController();
        setModel(model);
    }

    public Controller getController() {
        return this;
    }

    public void setView(View view) {
        this.view = view;

    }

    /**
     *
     * @return restart which is a flag to inform whether game is in state when
     * it can be restarted
     */
    public static boolean restartGame() {
        return restart;
    }
    /**
     * sets the restart to false, because game is turned on
     */
    public static void gameIsOn() {
        restart = false;
    }

    /**
     * restart is possible because the game has ended
     */
    public static void gameIsOff() {
        restart = true;
    }
    /**
     * Enables the newGame button
     */
    public void enableNewGameButton() {
        newGameButton.setEnabled(true);
    }
    
    /**
     * sends signal to model, to start new game
     */

    public void startNewGame() {
        gameIsOn();
        model.restartGame();
    }

    public void setButton(JButton button) {
        newGameButton = button;
    }

    public void setModel(Model model) {
        keyController.updateListener(model);
    }

    public static KeyController getKeyController() {
        if (keyController == null) {
            keyController = new KeyController();
        }
        return keyController;
    }
    /** 
     * sets pause in the game
     */
    public void setPause() {
        model.setPause();

    }

}
