/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.concurrent.BlockingQueue;
import model.Model;
import model.tetrominos.Tetromino;

/**
 * class which handles the keyboard input
 *
 * @author Filip
 */
public class KeyController implements KeyListener {

    private boolean[] keys;
    private Model model;

    public KeyController() {
        keys = new boolean[256];
        // this.queue = queue;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    public void updateListener(Model model) {
        this.model = model;
    }

    /**
     * keyController handles only arrows and space
     * send signals to model, to make actions on model
     * @param e key which has been pressed
     */
    @Override
    public void keyPressed(KeyEvent e) {
        keys[e.getKeyCode()] = true;

        if (keys[KeyEvent.VK_UP]) {
            model.rotateCurrentTetromino();
        }
        if (keys[KeyEvent.VK_DOWN]) {
            model.moveDownCurrentTetromino();
        }
        if (keys[KeyEvent.VK_LEFT]) {
            model.moveLeftCurrentTetromino();
        }
        if (keys[KeyEvent.VK_RIGHT]) {
            model.moveRightCurrentTetromino();
        }
        if (keys[KeyEvent.VK_SPACE]) {
            model.setPause();
        }

    }

    @Override
    public void keyReleased(KeyEvent e) {
        keys[e.getKeyCode()] = false;
    }

}
